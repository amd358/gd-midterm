﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;

public class Obstacle : MonoBehaviour {

	private PlatformerCharacter2D player; //calling character controller script

	// Use this for initialization
	void Start () {

		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<UnityStandardAssets._2D.PlatformerCharacter2D> (); //accessing platformercharacter2D script from tag "player"

	}

	void OnTriggerEnter2D(Collider2D col) {

		if (col.CompareTag ("Player")) { //if there is a collision between the obstacle's trigger area and the player, then...

			player.Damage (1); //take one heart from character's health

			StartCoroutine (player.Knockback (0.02f, 350, player.transform.position)); //start the IEnumerator Knockback, with specified values for each variable
		}

	}
}
