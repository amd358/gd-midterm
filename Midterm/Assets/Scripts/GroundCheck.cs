﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour {

	private CustomCharacterController character; //gives access to the script


	// Use this for initialization
	void Start () {
		character = gameObject.GetComponentInParent<CustomCharacterController> ();
	}
	

	void OnTriggerEnter2D (Collider2D col) {
		character.grounded = true;
	}


	void OnTriggerStay2D (Collider2D col) {
		character.grounded = true;
	}

	void OnTriggerExit2D (Collider2D col) {
		character.grounded = false;
	}
		

}
